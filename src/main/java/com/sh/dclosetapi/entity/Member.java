package com.sh.dclosetapi.entity;

import com.sh.dclosetapi.enums.MemberGrade;
import com.sh.dclosetapi.enums.MemberGroup;
import com.sh.dclosetapi.enums.PayDay;
import com.sh.dclosetapi.enums.PayWay;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Setter
@Getter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20, unique = true)
    private String userId;

    @Column(nullable = false, length = 16)
    private String memberPw;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false)
    private LocalDate birthDay;

    @Column(nullable = false, length = 13)
    private String phoneNumber;

    @Column(nullable = false, length = 30)
    private String memberAddress;

    @Column(nullable = false, length = 50)
    private String memberDetailedAddress;

    @Column(nullable = false)
    private Integer postCode;

    @Column(nullable = false)
    private LocalDate subscriptDate;

    @Column(nullable = false)
    private Boolean NYPersonalInfo;

    @Column(nullable = false)
    private Boolean NYMarketing;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private MemberGrade memberGrade;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private PayWay payWay;

    @Column(nullable = true, length = 50)
    private String payInfo;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private PayDay payDay;
}
