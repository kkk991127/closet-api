package com.sh.dclosetapi.model.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberResponse {
    private Long memberId;
    private String memberGroup;
    private String memberGrade;
}
