package com.sh.dclosetapi.model.questionbulletin;

import com.sh.dclosetapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter


public class QuestionBulletInChangeMemberRequest {


    private String questionTitle;
    private Integer questionPassword;
    private String questionContent;
}
