package com.sh.dclosetapi.controller;

import com.sh.dclosetapi.entity.Member;
import com.sh.dclosetapi.model.questionbulletin.*;
import com.sh.dclosetapi.service.MemberService;
import com.sh.dclosetapi.service.QuestionBulletInService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/questionBulletIn")
public class QuestionBulletInController {
    private final QuestionBulletInService questionBulletInService;
    private final MemberService memberService;

    // 등록 C
    @PostMapping ("/new/member-id/{memberId}")
    public String setQuestionBulletIn(@PathVariable long memberId,@RequestBody QuestionBulletInCreateRequest request){
        Member member = memberService.getMemberId(memberId);
        questionBulletInService.setQuestionBulletIn(member,request);
        return "ok";
    }

    // 복수 R
    @GetMapping("/list/all")
    public List<QuestionBulletInItem> getQuestionBulletIns() {
        return questionBulletInService.getQuestionBulletIns();
    }

    //단수 R
    @GetMapping("/detail/{id}")
    public QuestionBulletInResponse getQuestionBulletIn(@PathVariable long id) {return questionBulletInService.getQuestionBulletIn(id);}

    @GetMapping("/detail/member/{memberId}")
    public QuestionBulletInMemberResponse getQuestionMemberBulletIn(@PathVariable long memberId) {return questionBulletInService.getQuestionMemberBulletIn(memberId);}


    // id로 수정 하는 U
    @PutMapping("/change/correct-id/{id}")
        public String putQuestionBulletInChangeRequest(@PathVariable long id, @RequestBody QuestionBulletInChangeRequest request){
        questionBulletInService.putQuestionBulletInChangeRequest(id, request);

        return "ok";
    }

    // MemberId로 수정 하는 U
    @PutMapping("/correct/change-member/{memberId}")
    public String putQuestionBulletInChangeMemberRequest(@PathVariable long memberId, @RequestBody QuestionBulletInChangeMemberRequest request){
    questionBulletInService.putQuestionBulletInChangeMemberRequest(memberService.getMemberId(memberId), request);
        return "ok";
    }

    // 삭제 하는 D
    @DeleteMapping("/delete/{memberId}")
    public String delQuestionBulletIn(@PathVariable long memberId){
        questionBulletInService.delQuestionBulletIn(memberService.getMemberId(memberId));

        return "ok";
    }




}
