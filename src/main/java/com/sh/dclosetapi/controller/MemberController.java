package com.sh.dclosetapi.controller;
import com.sh.dclosetapi.model.member.MemberCreateRequest;
import com.sh.dclosetapi.model.member.MemberItem;
import com.sh.dclosetapi.model.member.MemberResponse;
import com.sh.dclosetapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @PostMapping("/new")
    public String setMember(@RequestBody MemberCreateRequest request) {
        memberService.setMember(request);
        return "OK";
    }

    @GetMapping("/all")
    public List<MemberItem> getMembers() {
        return memberService.getMembers();
    }


    public MemberResponse getMember(@PathVariable long id) {
        return memberService.getMember(id);
    }

}