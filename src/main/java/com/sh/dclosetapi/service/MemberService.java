package com.sh.dclosetapi.service;

import com.sh.dclosetapi.entity.Member;
import com.sh.dclosetapi.enums.MemberGrade;
import com.sh.dclosetapi.enums.MemberGroup;
import com.sh.dclosetapi.enums.PayDay;
import com.sh.dclosetapi.enums.PayWay;
import com.sh.dclosetapi.model.member.MemberCreateRequest;
import com.sh.dclosetapi.model.member.MemberItem;
import com.sh.dclosetapi.model.member.MemberResponse;
import com.sh.dclosetapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getMemberId(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setUserId(request.getUserId());
        addData.setMemberPw(request.getMemberPw());
        addData.setMemberName(request.getMemberName());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setMemberAddress(request.getMemberAddress());
        addData.setMemberDetailedAddress(request.getMemberDetailedAddress());
        addData.setPostCode(request.getPostCode());
        addData.setSubscriptDate(LocalDate.now());
        addData.setNYPersonalInfo(request.getNYPersonalInfo());
        addData.setNYMarketing(request.getNYMarketing());
        // 등급과 권한을 회원가입시 기본값 일반회원으로 고정.
        addData.setMemberGrade(MemberGrade.NORMAL);
        addData.setMemberGroup(MemberGroup.NORMAL);
        addData.setPayWay(PayWay.NONE);
        addData.setPayInfo(null);
        addData.setPayDay(PayDay.NONE);

        memberRepository.save(addData);
    }

    public List<MemberItem> getMembers() {
        List<Member> originList = memberRepository.findAll();
        List<MemberItem> result = new LinkedList<>();

        for(Member member : originList ) {
            MemberItem addItem = new MemberItem();
            addItem.setId(member.getId());
            addItem.setUserId(member.getUserId());
            addItem.setMemberName(member.getMemberName());
            addItem.setBirthDay(member.getBirthDay());
            addItem.setPhoneNumber(member.getPhoneNumber());
            addItem.setMemberAddress(member.getMemberAddress());
            addItem.setMemberDetailedAddress(member.getMemberDetailedAddress());
            addItem.setPostCode(member.getPostCode());
            addItem.setSubscriptDate(member.getSubscriptDate());
            addItem.setNYPersonalInfo(member.getNYPersonalInfo());
            addItem.setNYMarketing(member.getNYMarketing());
            addItem.setMemberGrade(member.getMemberGrade().getMemberGrade());
            addItem.setPayWay(member.getPayWay().getPayWay());
            addItem.setPayInfo(member.getPayInfo());
            addItem.setPayDay(member.getPayDay().getPayDay());

            result.add(addItem);
        }

        return result;
    }

    public MemberResponse getMember(long id) {
        Member originData = memberRepository.findById(id).orElseThrow();
        MemberResponse response = new MemberResponse();
        response.setMemberId(originData.getId());
        response.setMemberGrade(originData.getMemberGrade().getMemberGrade());
        response.setMemberGroup(originData.getMemberGroup().getMemberGroup());

        return response;
    }
}

