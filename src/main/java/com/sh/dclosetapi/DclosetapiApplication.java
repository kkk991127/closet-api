package com.sh.dclosetapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DclosetapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(DclosetapiApplication.class, args);
    }

}
