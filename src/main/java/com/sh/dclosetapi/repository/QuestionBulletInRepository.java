package com.sh.dclosetapi.repository;

import com.sh.dclosetapi.entity.QuestionBulletIn;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuestionBulletInRepository extends JpaRepository <QuestionBulletIn,Long> {
}
