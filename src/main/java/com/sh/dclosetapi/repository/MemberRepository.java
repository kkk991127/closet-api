package com.sh.dclosetapi.repository;

import com.sh.dclosetapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
